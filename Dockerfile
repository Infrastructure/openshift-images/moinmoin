FROM registry.access.redhat.com/ubi8/httpd-24:1

ENV MOIN_RELEASE=1.9.11

USER 0

RUN yum install python2-pip redhat-rpm-config \
    python2-devel gcc httpd-devel -y
RUN pip2 install mod-wsgi moin==${MOIN_RELEASE}

ADD app_data /tmp/src/
RUN chown -R 1001:0 /tmp/src

USER 1001
RUN /usr/libexec/s2i/assemble
CMD /usr/libexec/s2i/run
